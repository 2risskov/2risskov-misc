Juletur 2017
============

Her er planerne for juleturen i 2. Risskov spejdergruppe, november 2017.

Lørdag
------

* **1000~1200**: Ankomst ved Moesgaard Strandkiosk og gåtur til hytten.
  Spejderne får kort og kompas. Lederne griber kun ind, hvis de går helt forkert.

* **~1200-1230**: Frokost. Rugbrødsmadder, frugt og grønt.

* **1230-opgaven er klaret**: Pionering af et radiotårn.

* **efter pionering-1515**: Fri leg. Dem der skal overnatte må flytte deres ting ind på deres værelser.

* **1515-1600**: Flagregler, knob, alle skal prøve at hejse og hale et flag. Alle skal have prøvet at give flaget og at modtage det.

  Hurtigt recap af reglerne: Flaget må tidligst hejses klokken 0800, men hvis solen går op efter det, først ved solopgang. Flagstrygning skal ske inden solnedgang, men senest klokken 2000. Hænger Dannebrog ude om natten, "flager man for Fanden".

* **1600**: Flagstrygning

* **~1610-1630**: Fri leg og eftermiddagssnack: Frugt og grønt (måske varm kakao?). En af lederne har tændt bål allerede.

* **1630**: Morse fra radiotårnet. Det må gerne løbe ud i noget fritid for spejderne ved 1700-1715-tiden. 

* **1800**: Festbuffet som traditionen kræver det. :-)

* **bagefter**: Pakkeleg.



Søndag
------

* **0730**: Alt for tidligt op af sengen. Morgenmaden står klar allerede (Kira vil gerne stå for at sætte den ud). Efter morgenmaden må spejderne gerne gå i gang med at pakke deres ting.

* **0830**: Flaghejsning.

* **0840-0900**: Spejderne skal have tømt deres værelser.

  

Ellers ikke de store planer, siden vi forventer at have få spejdere. Flaget skal hejses ved solopgang. Radiotårnet skal pilles ned. Almen oprydning.

Hotdogs og afhentning (flaget skal hales) ved 1200-tiden.

